// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/no-unused-vars */
// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable jsx-a11y/no-static-element-interactions */
// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable jsx-a11y/click-events-have-key-events */
import './index.css'
import {
  Button,
  Modal,
  Form,
  FormControl,
  Textarea,
  Text,
  Notification,
} from '@contentful/f36-components'
import { useEffect, useState } from 'react'

import { Editor } from 'react-draft-wysiwyg'
import { EditorState } from 'draft-js'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import ImageUploader from 'react-images-upload'

import { Radio } from '@contentful/f36-forms'

const Header = ({ pages, setPages, saveTemplate }: any) => {
  /**
   * Open modal
   * @type {boolean}
   */
  const [modalOpen, setModalOpen] = useState(false)
  const [assetType, setAssetType] = useState('Image')
  const [editorState, setEditorState] = useState(EditorState.createEmpty())

  const [listOfAssetType, setListOfAssetsType] = useState([
    'Image',
    'Text',
    'Link',
  ])

  const submitForm = () => {
    console.log('sdfa')
  }

  /**
   * editore state changes
   * @param value
   */

  const onEditorStateChange = (value: any) => {
    setEditorState(value)
  }

  /**
   * Render content by asset type
   * @returns {any}
   */
  const renderContentByAssetType = () => {
    if (assetType == 'Image') {
      return (
        <div>
          {/* <ImageUploader
            withIcon={true}
            buttonText="Choose images"
            // onChange={onDrop}
            imgExtension={['.jpg', '.gif', '.png', '.gif']}
            maxFileSize={5242880}
            withPreview={true}
            singleImage={true}
          /> */}
        </div>
      )
    } else {
      return (
        <Editor
          editorState={editorState}
          wrapperClassName="demo-wrapper"
          editorClassName="demo-editor"
          onEditorStateChange={(value) => onEditorStateChange(value)}
        />
      )
    }
  }

  const renderModal = () => {
    return (
      <Modal onClose={() => setModalOpen(false)} isShown={modalOpen}>
        {() => (
          <>
            <Modal.Header
              title="Add Asset"
              onClose={() => setModalOpen(false)}
            />
            <Modal.Content>
              <div className="asset-root">
                <div className="asset-types-list">
                  {listOfAssetType.map((_x) => {
                    return (
                      <div key={_x} className="asset-types">
                        <Radio
                          value={_x}
                          isChecked={assetType === _x}
                          onChange={(e) => setAssetType(e.target.value)}
                        />
                        <Text>{_x}</Text>
                      </div>
                    )
                  })}
                </div>
                {renderContentByAssetType()}
              </div>
            </Modal.Content>
            <Modal.Controls>
              <Button
                size="small"
                variant="transparent"
                onClick={() => setModalOpen(false)}
              >
                Close
              </Button>
              <Button
                size="small"
                variant="positive"
                // isDisabled={link.length === 0}
                // onClick={() => uploadImage()}
              >
                Ok
              </Button>
            </Modal.Controls>
          </>
        )}
      </Modal>
    )
  }

  const renderPageNumber = () => {
    return (
      <div className="page-number">
        <div>
          {/* {pages.map((_x) => {
            return <div>{_x.pageNumber}</div>
          })} */}
          <div className="page-number">
            <div
              className={pages > 1 ? 'page-icon' : 'diabled page-icon'}
              onClick={() => (pages > 1 ? setPages(pages - 1) : '')}
            >
              &laquo;
            </div>
            <div>{pages}</div>
            <div className="page-icon" onClick={() => setPages(pages + 1)}>
              &raquo;
            </div>
          </div>
        </div>
      </div>
    )
  }

  return (
    <div className="header-root">
      <div className="application-name">Publitas</div>
      <div className="page-with-publication-name">
        <div className="application-name">Sample publitas</div>
        {renderPageNumber()}
      </div>
      <div className="add-button" style={{ gap: '20px', display: 'flex' }}>
        <Button
          variant="secondary"
          onClick={() => {
            saveTemplate()
            Notification.setPlacement('top')
            Notification.success('Template page Saved')
          }}
        >
          Save
        </Button>
        <Button variant="secondary" onClick={() => setModalOpen(true)}>
          Add Assets
        </Button>
      </div>
      {renderModal()}
    </div>
  )
}

export default Header
