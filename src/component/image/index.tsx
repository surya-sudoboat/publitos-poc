/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useState } from 'react'
import '../../root.css'
import { Resizable } from 're-resizable'
import {
  Asset,
  // Button,
  // Modal,
  Text,
  Collapse,
} from '@contentful/f36-components'
// import ImageUploader from 'react-images-upload'

const Image = ({ obj, setObj, imageAssets }: any) => {
  const [isExpanded, setIsExpanded] = useState(false)
  const [styles, setStyles] = useState({
    top: 0,
    left: 0,
    position: 'absolute',
    zIndex: 0,
    width: 200,
    height: 200,
  })

  useEffect(() => {
    console.log(obj)
  }, [obj])
  // console.log(obj

  // const [filedSelected, setFieldSelected] = useState(false)

  const [image, setImage] = useState<any>({
    src: '',
    alt: '',
    file: '',
    id: '',
  })

  // const [selecteFiled, setSelectedFiled] = useState()
  // const [modalOpen, setModalOpen] = useState(false)

  const [{ clientX, clientY }, setClient] = useState({
    clientX: 0,
    clientY: 0,
  })

  const handleDrop = (e: any, src: any, id: any) => {
    e.preventDefault()
    const clientRect = e.currentTarget.getBoundingClientRect()

    if (clientRect.top + e.clientY - clientY > 0) {
      const imgSrc = { ...image, id: Math.random() }
      setImage(imgSrc)
      const stylesObj = {
        left: clientRect.left + e.clientX - clientX,
        top: clientRect.top + e.clientY - clientY,
        position: 'fixed',
        zIndex: 0,
      }
      setStyles(stylesObj)
      dropTheElement(stylesObj, src, imgSrc)
    }
  }

  const dropTheElement = (stylesObj: any, src: any, imgSrc: any) => {
    const children = obj?.children
    console.log(children)

    const childEle = {
      name: 'image',
      id: imgSrc?.id,
      styles: stylesObj,
      value: src,
      type: 'image',
      //
    }
    setStyles(stylesObj)

    children.push(childEle)
    setObj({ ...obj, children: children })
    setClient({
      clientX: 0,
      clientY: 0,
    })
  }

  return (
    <>
      <div className="text-root">
        <div className="accordian" onClick={() => setIsExpanded(!isExpanded)}>
          <Text style={{ width: '100%' }}>Image Assets</Text>
        </div>

        <Collapse isExpanded={isExpanded}>
          <div className="image-assets">
            {imageAssets.map((_x: any, index: any) => {
              return (
                <div
                  key={_x}
                  className="image-drag-drop"
                  draggable={true}
                  onDragOver={(e) => e.preventDefault()}
                  onDragStart={(e) => {
                    setClient({ clientX: e.clientX, clientY: e.clientY })
                  }}
                  onDragEndCapture={(e) => {
                    handleDrop(e, _x, index)
                  }}
                >
                  <div style={{ width: '100%', height: '100%' }}>
                    <Asset
                      style={{ width: '100%', height: '100%' }}
                      src={_x}
                      type="image"
                    />
                  </div>
                </div>
              )
            })}
          </div>
        </Collapse>
      </div>
    </>
  )
}

export default Image
