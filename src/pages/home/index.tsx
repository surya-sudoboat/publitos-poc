import React, { useEffect, useState } from 'react'
import DragComponet from '../../component/dragBody'

import Header from '../../component/Header'
import './index.css'
import cloneDeep from 'clone-deep'

const HomePage = () => {
  const [pages, setPages] = useState(1)
  const [pageContent, setPageContent] = useState<any>([])
  const [obj, setObj] = useState<any>({
    id: pages,
    children: [],
  })

  useEffect(() => {
    const data = pageContent.filter((_x: any) => _x.id == pages)
    if (data?.length > 0) {
      setObj(data[0])
    } else {
      setObj({ id: pages, children: [] })
    }
  }, [pageContent, pages])

  useEffect(() => {
    console.log(obj)
  }, [obj])

  useEffect(() => {
    const data: any = localStorage.getItem('contnet')
    console.log(data)
    if (data) {
      setPageContent(JSON.parse(data))
    }
  }, [])

  /**
   * Save template
   
   */

  const saveTemplate = () => {
    const pageData = cloneDeep(pageContent)
    let isAlready = { value: false, index: '' }
    pageData.forEach((_x: any, index: any) => {
      if (_x?.id == pages) {
        isAlready = { value: true, index: index }
      }
    })
    if (isAlready?.value) {
      pageData[isAlready?.index] = obj
    } else {
      pageData.push(obj)
    }
    setObj({ id: pages, children: [] })
    setPageContent(pageData)
    localStorage.setItem('content', JSON.stringify(pageData))
  }

  useEffect(() => {
    console.log('pageContent', pageContent)
  }, [pageContent])

  useEffect(() => {
    console.log(pageContent)
  }, [pageContent])
  return (
    <div className="root-div">
      <div>
        <Header
          pages={pages}
          setPages={setPages}
          saveTemplate={() => saveTemplate()}
        />
      </div>
      <div style={{ height: 'calc( 100vh - 150px )', width: '100%' }}>
        <DragComponet
          pages={pages}
          setPages={setPages}
          pageContent={pageContent}
          setPageContent={setPageContent}
          obj={obj}
          setObj={setObj}
        />
        {/* <DropComponet /> */}
      </div>
    </div>
  )
}

export default HomePage
