/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { useState } from 'react'
import './index.css'
import Block from '../block'
import Image from '../image'
import Text from '../Text'
import Video from '../video'
import Link from '../link'

const DropComponet = () => {
  return (
    <div className="drop-componet">
      {/* <div>This is drag</div> */}
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: '10px',
          padding: '10px',
        }}
      >
        <Block />
        <Image />
        <Text />
        <Link />
        <Video />
      </div>
    </div>
  )
}

export default DropComponet
