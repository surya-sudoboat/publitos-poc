/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import './index.css'

import Container from './treeElement'

const Tree = ({ obj, setObj, setSelectedId, selectedId }: any) => {
  return (
    <div>
      <div className="layout-section">Layout</div>
      <Container
        obj={obj}
        setObj={setObj}
        setSelectedId={setSelectedId}
        selectedId={selectedId}
      />
    </div>
  )
}

export default Tree
