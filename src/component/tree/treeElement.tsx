/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import CardList from './cardList'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import { useEffect } from 'react'
import cloneDeep from 'clone-deep'

const Container = ({ obj, setObj, setSelectedId, selectedId }: any) => {
  const handleOnDragEnd = (e: any) => {
    console.log(e)
    const dropedData = obj?.children
    const [removed] = dropedData.splice(e?.source?.index, 1)
    dropedData.splice(e?.destination?.index, 0, removed)
    setObj({ ...obj, children: dropedData })
  }

  const deleteElement = (id: any) => {
    const child = cloneDeep(obj?.children)

    const updatedData: any = {
      id: obj?.id,
      children: child.filter((item: any) => item?.id !== id),
    }
    console.log(updatedData)

    setObj(updatedData)
  }

  return (
    <DragDropContext onDragEnd={(e) => handleOnDragEnd(e)}>
      <Droppable droppableId="droppable">
        {(provided, snapshot) => (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            style={{ display: 'flex', flexDirection: 'column', gap: '5px' }}

            // style={getListStyle(snapshot.isDraggingOver)}
          >
            {(obj?.children || []).map((item: any, index: any) => (
              <Draggable
                key={item.id}
                draggableId={item.id.toString()}
                index={index}
              >
                {(provided, snapshot) => (
                  // eslint-disable-next-line jsx-a11y/no-static-element-interactions
                  <div
                    onDragEnterCapture={(e) => console.log(e)}
                    draggable={true}
                    className={
                      selectedId == item?.id
                        ? ' list-root selected'
                        : 'list-root'
                    }
                    onClick={() => setSelectedId(item.id)}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    // style={getItemStyle(
                    //   snapshot.isDragging,
                    //   provided.draggableProps.style
                    // )}
                  >
                    <CardList
                      obj={item}
                      setObj={setObj}
                      deleteElement={deleteElement}
                    />
                  </div>
                )}
              </Draggable>
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  )
}

export default Container
