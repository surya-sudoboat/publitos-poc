/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { useState } from 'react'
import '../../root.css'

import { Resizable } from 're-resizable'
import ReactPlayer from 'react-player'
import {
  Button,
  Modal,
  FormControl,
  Form,
  Textarea,
  Text,
} from '@contentful/f36-components'

const Video = ({ obj, setObj }: any) => {
  const [styles, setStyles] = useState({
    top: 0,
    left: 0,
    width: '100px',
    height: '100px',
    position: 'retlative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  })

  const [{ clientX, clientY }, setClient] = useState({
    clientX: 0,
    clientY: 0,
  })
  const handleDrop = (e: any) => {
    const clientRect = e.currentTarget.getBoundingClientRect()
    const stylesObj = {
      left: clientRect.left + e.clientX - clientX,
      top: clientRect.top + e.clientY - clientY,
      position: 'absolute',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100px',
      height: '100px',
    }
    dropTheElement(stylesObj)
  }

  const dropTheElement = (stylesObj: any) => {
    const children = obj?.children
    const childEle = {
      name: 'image',
      styles: stylesObj,
      body: renderImageSection(),
    }
    setStyles(stylesObj)
    children.push(childEle)
    setObj({ ...obj, children: children })
  }

  const renderImageSection = () => {
    return (
      <Resizable defaultSize={{ width: '100px', height: '100px' }}>
        <div draggable={true}>
          <div>
            <ReactPlayer
              width="100%"
              height="100%"
              padding="5px"
              controls={false}
              url={'http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4'}
              config={{ file: { attributes: { controlsList: 'nodownload' } } }}
            />
          </div>
        </div>
      </Resizable>
    )
  }

  return (
    <Resizable
      defaultSize={{
        width: 100,
        height: 100,
      }}
    >
      <div
        className="image-drag-drop"
        draggable={true}
        onDragStart={(e) => {
          setClient({ clientX: e.clientX, clientY: e.clientY })
        }}
        onDragEndCapture={(e) => {
          handleDrop(e)
        }}
        onDrag={(e) => e.preventDefault()}
        onDragOver={(e) => e.preventDefault()}
        // onDragEndCapture={(e) => handleDrop(e)}

        // onDragEnd={(e) => handleDrop(e)}
      >
        <Text>Video</Text>
      </div>
    </Resizable>
  )
}

export default Video
