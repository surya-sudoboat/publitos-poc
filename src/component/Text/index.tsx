/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { useState } from 'react'
import '../../root.css'
// import { Resizable } from 're-resizable'
import { Text, Collapse } from '@contentful/f36-components'

const TextSection = ({ obj, setObj, textAssets }: any) => {
  const [isExpanded, setIsExpanded] = useState(false)
  const [styles, setStyles] = useState({
    top: 0,
    left: 0,
    width: '100px',
    height: '40px',
    position: 'absolute',

    // zIndex: '100',
  })

  const [{ clientX, clientY }, setClient] = useState({
    clientX: 0,
    clientY: 0,
  })

  const handleDrop = (e: any, value: any, id: any) => {
    const clientRect = e.currentTarget.getBoundingClientRect()
    if (clientRect.top + e.clientY - clientY > 80) {
      const stylesObj = {
        left: clientRect.left + e.clientX - clientX,
        top: clientRect.top + e.clientY - clientY,
        position: 'absolute',

        height: '40px',
        // zIndex: '100',
      }
      dropTheElement(stylesObj, value)
    }
  }

  const dropTheElement = (stylesObj: any, value: any) => {
    const children = obj?.children
    const childEle = {
      name: 'text',
      styles: stylesObj,
      id: Math.random(),
      type: 'text',
      value: value,
      // body: renderImageSection(value, id),
    }
    setStyles(stylesObj)
    children.push(childEle)
    setObj({ ...obj, children: children })
    setClient({ clientX: 0, clientY: 0 })
  }

  return (
    <div className="text-root">
      <div className="accordian" onClick={() => setIsExpanded(!isExpanded)}>
        <Text style={{ width: '100%' }}>Text Assets</Text>
      </div>
      <Collapse isExpanded={isExpanded} className="text-area">
        <div className="text-drag-drop">
          {textAssets.map((_x: any, index: any) => {
            return (
              <div
                key={index}
                draggable={true}
                onDragStart={(e) => {
                  setClient({ clientX: e.clientX, clientY: e.clientY })
                }}
                // onDragOver={(e) => console.log(e)}
                onDragEndCapture={(e) => {
                  handleDrop(e, _x, index)
                }}
              >
                <div className="text">
                  <Text>{_x}</Text>
                </div>
              </div>
            )
          })}
        </div>
      </Collapse>
    </div>
  )
}

export default TextSection
