// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable eslint-comments/no-duplicate-disable */
/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect, useState } from 'react'
import './index.css'
import GridLines from 'react-gridlines'

import { Resizable } from 're-resizable'

import { Text } from '@contentful/f36-components'
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch'

// import { Resizable, ResizableBox } from 'react-resizable'

import Image from '../image'
import TextSection from '../Text'

import cloneDeep from 'clone-deep'

import Tree from '../tree'
import { Editor } from 'react-draft-wysiwyg'
import { EditorState } from 'draft-js'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'

import { SmartBlock, Extensions } from 'smartblock'

const DragComponet = ({
  pages,
  setPages,
  pageContent,
  setPageContent,
  obj,
  setObj,
}: any) => {
  const [{ clientX, clientY }, setClient] = useState({
    clientX: 0,
    clientY: 0,
  })

  const [editorState, setEditorState] = useState(EditorState.createEmpty())

  // useEffect(() => {
  //   console.log(pages)
  //   if (pages > 1) {
  //     const pageData = cloneDeep(pageContent)
  //     let isAlready = { value: false, index: '' }
  //     pageData.forEach((_x: any, index: any) => {
  //       console.log(_x, pages)
  //       console.log(_x?.id == pages)
  //       if (_x?.id == pages) {
  //         isAlready = { value: true, index: index }
  //       }
  //     })
  //     if (isAlready?.value) {
  //       console.log('this is value')
  //       pageData[isAlready?.index] = obj
  //     } else {
  //       pageData.push(obj)
  //     }
  //     setObj({ id: pages, children: [] })
  //     setPageContent(pageData)
  //   }

  //   // if (pageData) pageData.push(obj)
  // }, [pages])

  // useEffect(() => {
  //   const item: any = localStorage.getItem('content')
  //   if (item) {
  //     setObj(JSON.parse(item))
  //   }
  // }, [])

  const [selectedId, setSelectedId] = useState('')

  const [assets, setAssets] = useState({
    imageAssets: [
      'src/asset/image_1.png',
      'src/asset/image_2.png',
      'src/asset/image_3.png',
      'src/asset/image_4.png',
    ],
    textAsset: [
      'The sample Text 1',
      'The sample Text 2',
      'The sample Text 3',
      'The sample Text 4',
      'The sample Text 5',
    ],
  })

  const handleDrop = (e: any, id: any) => {
    const clientRect = e.currentTarget.getBoundingClientRect()

    if (clientRect.top + e.clientY - clientY > 80) {
      const stylesObj = {
        left: clientRect.left + e.clientX - clientX,
        top: clientRect.top + e.clientY - clientY,
        position: 'fixed',
      }
      const children = obj?.children
      let isAlreday: any = { id: '', index: '' }
      children.map((_x: any, index: any) => {
        if (_x?.id == id) {
          isAlreday = { id: _x?.id, index: index }
        }
      })
      if (isAlreday?.index > -1) {
        const childEle = {
          name: children[isAlreday?.index]?.name,
          type: children[isAlreday?.index]?.type,
          styles: stylesObj,
          id: isAlreday?.id,
          value: children[isAlreday?.index]?.value,
        }
        console.log(childEle, isAlreday?.index)
        children[isAlreday?.index] = childEle
      }

      setObj({ ...obj, children: children })
    }
  }

  // delete  element from the editor
  const deleteElement = (id: any) => {
    const child = cloneDeep(obj?.children)

    const updatedData: any = {
      id: obj?.id,
      children: child.filter((item: any) => item?.id !== id),
    }

    setObj(updatedData)
  }

  //image section based on type

  const renderImageSection = (src: any, id: any) => {
    return (
      <div id={id} style={{ width: '100%', height: '100%' }}>
        <div
          style={{
            width: '100%',
            height: '100%',
            background: 'red',
            backgroundImage: `url(${src})`,
            backgroundSize: 'cover',
          }}
        ></div>
      </div>
    )
  }

  //text section based on type
  const renderTextSection = (value: any, id: any) => {
    return (
      // <Resizable defaultSize={{ width: '100px', height: '100px' }}>
      <div
        id={id}
        // draggable={true}

        style={{ minWidth: '100px', minHeight: '50px' }}
      >
        <Text className="text">{value}</Text>
      </div>
      // </Resizable>
    )
  }

  //render selected element
  /**
   *render selected element
   * @param _x :any
   * @returns:any
   */
  const renderElementSection = (_x: any) => {
    if (_x?.type == 'image') {
      return renderImageSection(_x?.value, _x?.id)
    } else if (_x?.type == 'text') {
      return renderTextSection(_x?.value, _x?.id)
    }
  }

  const resizeElement = (ref: any, id: any) => {
    const child = cloneDeep(obj?.children)
    const pageData = cloneDeep(pageContent)

    let isAlready = { index: '', value: false }
    let isAvailable: any
    child.forEach((_x: any, ind: any) => {
      if (_x?.id == id) {
        isAlready = { index: ind, value: true }
      }
    })
    if (isAlready?.value) {
      child[isAlready?.index]['styles'] = {
        left: child[isAlready?.index]['styles'].left,
        top: child[isAlready?.index]['styles'].top,
        position: child[isAlready?.index]['styles'].position,
        zIndex: child[isAlready?.index]['styles'].zIndex,
        width: ref.clientWidth,
        height: ref.clientHeight,
      }
    }

    setObj({ id: pages, children: child })
    pageData.forEach((_x: any, index: any) => {
      console.log(_x?.id, pages)
      if (_x?.id == pages) {
        isAvailable = index
      }
    })
    console.log(isAvailable > -1, 'this is true', isAvailable)
    if (isAvailable > -1) {
      console.log(isAvailable, 'isAvaliable')
      pageData[isAvailable] = { id: pages, children: child }
      console.log(pageData[isAvailable], 'pageData[isAvailable]')
      console.log(pageData, 'pageData')
      setPageContent(pageData)
    }

    // console.log(child, 'child')
    // setObj({ id: pages, children: child })

    // setObj({ ...obj, updatedData })
  }

  return (
    <div
      id="main-frame"
      style={{ height: '100%', display: 'flex', gap: '10px' }}
    >
      <div className="tree-section">
        <Tree
          obj={obj}
          setObj={setObj}
          setSelectedId={setSelectedId}
          selectedId={selectedId}
        />
      </div>
      <div style={{ width: '70%', margin: 'auto' }}>
        <div className="drag-componet" id="main-frame-1">
          <TransformWrapper
            initialScale={1}
            disabled={false}
            minScale={0.5}
            // maxScale={1}
            limitToBounds={false}
            // onPanning={updateXarrow}
            // onZoom={updateXarrow}
            pinch={{ step: 5 }}
          >
            <TransformComponent
              contentClass="drag-componet-zoom"
              wrapperStyle={{ height: '100%', width: '100%' }}
            >
              <GridLines
                className="grid-area"
                cellWidth={10}
                strokeWidth={1}
                strokeWidth2={0.5}
                cellWidth2={2}
                dashArray="2"
                dashArray2="1"
              >
                {(obj?.children || []).map((_x: any, index: any) => {
                  return (
                    <div
                      style={_x?.styles}
                      key={_x?.id}
                      id={_x?.id}
                      onClick={() => setSelectedId(_x?.id)}
                    >
                      <div style={{ position: 'relative' }}>
                        {selectedId == _x?.id && (
                          <div
                            style={{
                              display: 'flex',
                              width: '100%',
                              justifyContent: 'flex-end',
                            }}
                          >
                            <div
                              onClick={() => deleteElement(_x?.id)}
                              style={{
                                width: 'fit-content',
                                border: '1px solid rgb(62, 161, 227)',
                                padding: '3px',
                                fontFamily: 'fantasy',
                                fontSize: '10px',
                                background: ' rgb(62, 161, 227)',
                                position: 'absolute',
                                // fontSize: '10px',
                                // position: 'absolute',
                                zIndex: 1,
                                cursor: 'pointer',
                              }}
                            >
                              X
                            </div>
                          </div>
                        )}

                        <div className={selectedId == _x?.id ? 'selected' : ''}>
                          <Resizable
                            defaultSize={{
                              width: 200,
                              height: _x?.name == 'text' ? '' : 200,
                            }}
                            // onResizeStop={(e, direction, ref, d) => {
                            //   resizeElement(ref, _x?.id)
                            // }}
                          >
                            <div
                              style={{ width: '100%', height: '100%' }}
                              draggable={true}
                              // onClick={(e) => e.stopPropagation()}
                              onDragOver={(e) => e.preventDefault()}
                              onDragStart={(e) => {
                                setClient({
                                  clientX: e.clientX,
                                  clientY: e.clientY,
                                })
                              }}
                              onDragEndCapture={(e) => handleDrop(e, _x?.id)}
                              key={_x?.id}
                            >
                              {renderElementSection(_x)}
                            </div>
                          </Resizable>
                        </div>
                      </div>
                    </div>
                  )
                })}
              </GridLines>
            </TransformComponent>
          </TransformWrapper>
        </div>
      </div>

      <div className="drop-componet">
        {/* <div>This is drag</div> */}
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '10px',
            padding: '10px',
          }}
        >
          <Image obj={obj} setObj={setObj} imageAssets={assets?.imageAssets} />
          <TextSection
            obj={obj}
            setObj={setObj}
            textAssets={assets?.textAsset}
          />
          {/* <Link obj={obj} setObj={setObj} />
          <Video obj={obj} setObj={setObj} /> */}
        </div>
      </div>
    </div>
  )
}

export default DragComponet
