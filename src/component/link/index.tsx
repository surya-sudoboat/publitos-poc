import { useEffect, useState } from 'react'
import '../../root.css'
import { Resizable } from 're-resizable'

import {
  Button,
  Modal,
  FormControl,
  Form,
  Text,
  TextInput,
} from '@contentful/f36-components'

const Link = ({ obj, setObj }: any) => {
  const [styles, setStyles] = useState({
    top: 0,
    left: 0,
    width: '200px',
    height: '40px',
    position: 'retlative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  })

  const [link, setLink] = useState('')

  const [modalOpen, setModalOpen] = useState(false)

  const [{ clientX, clientY }, setClient] = useState({
    clientX: 0,
    clientY: 0,
  })
  const handleDrop = (e: any) => {
    // console.log(e)
    const clientRect = e.currentTarget.getBoundingClientRect()
    const stylesObj = {
      left: clientRect.left + e.clientX - clientX,
      top: clientRect.top + e.clientY - clientY,
      position: 'absolute',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '200px',
      height: '40px',
    }
    dropTheElement(stylesObj)
  }

  const submitForm = () => {
    if (link.length === 0) {
      return
    }
    // const list = obj?.children
    // list.forEach((_x)=>{

    // })
    setModalOpen(false)
  }

  const dropTheElement = (stylesObj: any) => {
    const children = obj?.children
    const childEle = {
      name: 'image',
      styles: stylesObj,
      body: renderImageSection(),
      id: Math.random(),
    }
    setStyles(stylesObj)
    children.push(childEle)
    setObj({ ...obj, children: children })
  }

  const renderImageSection = () => {
    return (
      <Resizable defaultSize={{ width: '200px', height: '40px' }}>
        <div draggable={true} className="link-section">
          {link?.length > 0 ? (
            <Text>{link}</Text>
          ) : (
            <div onClick={() => setModalOpen(true)}>Add Link</div>
          )}
        </div>
      </Resizable>
    )
  }

  const renderModal = () => {
    return (
      <Modal onClose={() => setModalOpen(false)} isShown={modalOpen}>
        {() => (
          <>
            <Modal.Header
              title="Add Link"
              onClose={() => setModalOpen(false)}
            />
            <Modal.Content>
              <Form onSubmit={submitForm}>
                <FormControl>
                  <FormControl.Label isRequired> Link</FormControl.Label>
                  <TextInput
                    maxLength={20}
                    value={link}
                    placeholder="For example www.ikea.com, www.google.com,..."
                    onChange={(e) => {
                      setLink(e.target.value)
                    }}
                  />
                </FormControl>
              </Form>
            </Modal.Content>
            <Modal.Controls>
              <Button
                size="small"
                variant="transparent"
                onClick={() => setModalOpen(false)}
              >
                Close
              </Button>
              <Button
                size="small"
                variant="positive"
                isDisabled={link.length === 0}
                onClick={submitForm}
              >
                Create
              </Button>
            </Modal.Controls>
          </>
        )}
      </Modal>
    )
  }

  return (
    <>
      <div
        className="image-drag-drop"
        draggable={true}
        onDragStart={(e) => {
          setClient({ clientX: e.clientX, clientY: e.clientY })
        }}
        onDragEndCapture={(e) => {
          handleDrop(e)
        }}
      >
        <div>
          <Text>Link</Text>
        </div>
      </div>
      {renderModal()}
    </>
  )
}

export default Link
