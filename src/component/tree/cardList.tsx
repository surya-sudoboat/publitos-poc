/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Text } from '@contentful/f36-components'

const CardList = ({ obj, index, setObj, deleteElement }: any) => {
  return (
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      <Text className="list-content">{obj?.name}</Text>
      <div
        onClick={(e) => {
          e.stopPropagation()
          deleteElement(obj?.id)
        }}
        style={{ paddingRight: '5px', fontFamily: 'fantasy' }}
      >
        X
      </div>
    </div>
  )
}

export default CardList
