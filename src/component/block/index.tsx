import React, { useState } from 'react'

const Block = () => {
  const [styles, setStyles] = useState({
    top: 0,
    left: 0,
    position: 'retlative',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  })

  const [{ clientX, clientY }, setClient] = useState({
    clientX: 0,
    clientY: 0,
  })
  const handleDrop = (e: any) => {
    const clientRect = e.currentTarget.getBoundingClientRect()
    setStyles({
      left: clientRect.left + e.clientX - clientX,
      top: clientRect.top + e.clientY - clientY,
      position: 'absolute',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    })
  }
  return (
    <div
      style={styles}
      draggable={true}
      onDragStart={(e) => {
        setClient({ clientX: e.clientX, clientY: e.clientY })
      }}
      onDragEndCapture={(e) => {
        handleDrop(e)
      }}
      onDrag={(e) => e.preventDefault()}
      onDragOver={(e) => e.preventDefault()}
    >
      <div style={{ width: '100%', height: '100%' }}>Block</div>
    </div>
  )
}

export default Block
